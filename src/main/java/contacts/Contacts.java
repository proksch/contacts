/**
 * Copyright 2021 Sebastian Proksch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package contacts;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Contacts {

	private Set<Person> people;

	public void add(Person p) {
		if (p == null) {
			throw new RuntimeException("Person is null");
		}
		if (people.contains(p)) {
			throw new RuntimeException("Trying to add an existing person");
		}
		people.add(p);
	}

	public void remove(Person p) {
		if (p == null) {
			throw new RuntimeException("Person is null");
		}
		if (!people.contains(p)) {
			throw new RuntimeException("Trying to remove a non-existing person");
		}
		people.remove(p);

	}

	public List<String> getAllNames() {
		return people.stream()//
				.map(p -> p.name)//
				.collect(Collectors.toList());
	}
}