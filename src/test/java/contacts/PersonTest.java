/**
 * Copyright 2021 Sebastian Proksch
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package contacts;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class PersonTest {

	@Test
	public void initializationIsCorrect() {
		Person sut = new Person("a", 1, Gender.MALE);
		assertEquals("a", sut.name);
		assertEquals(1, sut.age);
		assertEquals(Gender.MALE, sut.gender);
	}

	@Test
	public void equalsIdentical() {
		Person a = new Person("a", 1, Gender.MALE);
		Person b = new Person("a", 1, Gender.MALE);
		assertEquals(a, b);
		assertEquals(a.hashCode(), b.hashCode());
	}

	@Test
	public void equalsDifferentName() {
		Person a = new Person("a", 1, Gender.MALE);
		Person b = new Person("b", 1, Gender.MALE);
		assertNotEquals(a, b);
		assertNotEquals(a.hashCode(), b.hashCode());
	}

	@Test
	public void equalsDifferentAge() {
		Person a = new Person("a", 1, Gender.MALE);
		Person b = new Person("a", 2, Gender.MALE);
		assertNotEquals(a, b);
		assertNotEquals(a.hashCode(), b.hashCode());
	}

	@Test
	public void equalsDifferentGender() {
		Person a = new Person("a", 1, Gender.MALE);
		Person b = new Person("a", 1, Gender.FEMALE);
		assertNotEquals(a, b);
		assertNotEquals(a.hashCode(), b.hashCode());
	}

	@Test
	public void toStringImplemented() {
		Person a = new Person("a", 1, Gender.MALE);
		assertEquals("Person(a, 1, MALE)", a.toString());
	}
}